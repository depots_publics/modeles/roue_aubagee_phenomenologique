# -*- coding: utf-8 -*-
"""
Calculer la réponse forcée d'un modèle de roue aubagée phénoménologique : chaîne d'oscillateurs couplés à 1 degré de liberté

Source modèle : Pierre et Wei (1988) Ottarsson et Pierre (1995) Sinha (2005)

@author: jdreau
"""

"Modules"
import numpy as np
from scipy.linalg import eigh as scipy_eigh
from scipy.sparse import spdiags
from math import sqrt, pi
from cmath import exp
from numba import jit
import matplotlib.pyplot as plt     # Tracé de courbes

def lecture(Modele):
    "lecture des paramètres d'entrée"
    "Attention : encodage utf-8 à ajouter : open(file.txt, 'r', encoding='utf-8') "
    local_res = locals()     
    exec(open('params_RA.py','r', encoding="utf-8").read(), globals(), local_res) 
    Sollic = local_res["Sollic"]
    Visu = local_res["Visu"]
    return Modele, Sollic, Visu


def syst(N_a, m_b, k_b, k_c, delta_E ) :
    """
        Construction des matrices M et K :
            M = m_b I
            K = A0 + dA,  avec A0 = k_b diags (1+delta_E) et terme de couplage dA = k_c circulante(2, -1, 0, ... , 0, -1)
    """    
    "Matrice de masse " 
    M = m_b * np.eye(N_a)
    
    "Matrice de raideur"
    A0 = k_b * spdiags( 1 + delta_E, [0], N_a, N_a) #sparse diags
    
    # Terme de couplage, matrice circulante : 2 -1 0 .. 0 -1 
    e =  np.ones(N_a)
    if N_a > 2 : 
        data = np.array([-e, -e , 2*e , -e, -e]) 
        num_diags = [-N_a+1,-1, 0, 1,N_a-1]
    elif N_a == 2 :
        data = np.array([-e , 2*e , -e]) # matrice : [2 - ; - 2]
        num_diags = [-1, 0, 1]
    else : #N_a == 1
        data = np.array([2*e]) # matrice : [2R]
        num_diags = [0]
    dA = k_c * spdiags(data, num_diags, N_a, N_a)
    
    K = A0 + dA
    
    return M, K.toarray()


def fct_vp(K, M):
    """ Résolution d'un système aux valeurs propres :  
            K U = w**2 M U, U: vecteurs propres, w**2 : valeurs propres
            /!\ hypothèse K est symétrique pour utiliser eigh
    """    
    valp, vecp = scipy_eigh(K, b=M)
    #considéré comme un réel si la partie imaginaire est inférieure à 1e-10 (sinon Python l'inscrit toujours)
    vecp = np.real_if_close(vecp, tol=1e+6) 
    valp = np.real_if_close(valp, tol=1e+6)
    
    return valp, vecp


def fct_base_mod(vecp, M, K, C, F):
    """
        Décomposition dans la base modale
            Phi : matrice des vecteurs propres normalisés : Phi[:,s] = vecp[:,s] / sqrt( vecp[:,s].T M  vecp[:,s]  )
    """
    
    "Base modale, choix de la norme : M-norme "
    M_mod = np.dot(np.dot(vecp.T,M),vecp) 
    norme = (np.diag(M_mod))**0.5
    Phi = vecp / norme
    
    "Matrice de masse modale "
    M_mod = np.dot(np.dot(Phi.T,M),Phi) #Identité : np.eye(Modele['N_a'])
    
    "Matrice de raideur modale"
    K_mod = np.dot(np.dot(Phi.T,K),Phi) # omega ^2 I  : omega_i**2 * np.eye(Modele['N_a']) 
    
    "Matrice d'amortissement modale "
    C_mod = np.dot(np.dot(Phi.T,C),Phi) # 2 xi omega I : 2 * ksi * omega_i * np.eye(Modele['N_a'])
    
    "Vecteur des forces d'excitation modal"
    F_mod = np.dot(Phi.T, F) #Phi.T F
    
    return M_mod, K_mod, C_mod, F_mod, Phi
    
@jit(nopython = True)
def fct_calcul_FRF(freq_tir,  F_mod, F_modAc, Phi, PhiAc, ksi, ksiAc, omega_i, omega_acc_i, u_mod_desacc, q_mod_desacc, u_mod_acc, q_mod_acc):
    """
        Réponse forcée par décomposition modale
            système initial : M d2u(t) + C du(t) + K u(t) = f(t)
            décomposition dans la base : u = Phi x
                avec Phi : matrice des vecteurs propres normalisés : Phi[:,s] = vecp[:,s] / sqrt( vecp[:,s].T M  vecp[:,s]  )
            système modal : M_mod d2x(t) + C_mod dx(t) + K_mod x(t) = f_mod(t)
                avec M_mod = Id
                    C_mod = 2 kxi omega_i Id
                    K_mod = omega_i^2 Id
                    f_mod(t) = Phi.T f(t)
            sous hypothèse harmonique x(t) = X exp(1j omega t)
    """    
    for i, freq in enumerate(freq_tir):    
        omega = freq*2*pi   #omega excitation :  ω = 2 pi freq        
        
        "--------------------------------------------------------"
        "1 : Système désaccordé"
        "Réponse forcée"    
        Dcplx =  omega_i**2 - omega**2 + 2*1j*ksi * omega_i * omega
        mat_Dcplx = np.diag(Dcplx)
        X = np.linalg.solve( mat_Dcplx , F_mod.astype(type(1j))  ) #[:,0]
        "RQ : astype(type(1j)) est ajouté pour que mat_Dcplx et F_mod ont le même type (complex), jit ne peut pas convertir F_mod en complex lors du solve et du dot"
        
        "Amplitude physique complexe"    
        U = np.dot(Phi.astype(type(1j)) , X)
        u_mod_desacc[i] = U         # amplitude complexe
        q_mod_desacc[i] = np.abs(U) # amplitude réelle
        
        "--------------------------------------------------------"
        "2 : Système accordé"   
        "Réponse forcée"
        DcplxAc =  omega_acc_i**2 - omega**2 + 2*1j*ksiAc * omega_acc_i * omega
        mat_DcplxAc = np.diag(DcplxAc)
        Xac = np.linalg.solve( mat_DcplxAc , F_modAc.astype(type(1j))) #[:,0] 
        
        "Amplitude physique complexe"    
        Uac = np.dot(PhiAc.astype(type(1j)) , Xac)
        u_mod_acc[i] = Uac         # amplitude complexe
        q_mod_acc[i] = np.abs(Uac)  # amplitude réelle
        
        """
            Remarques : autres écritures équivalentes du calcul de la réponse forcée
            Pb_mod = - omega**2 * M_mod + 1j * omega * C_mod + K_mod
            X = np.linalg.solve(Pb_mod, F_mod.astype(type(1j)))
            U = np.dot(Phi.astype(type(1j)),X2)
            
            Pb_init = - omega**2 * Syst['M'] + 1j * omega * Syst['C'] + Syst['K']
            U = np.linalg.solve(Pb_init, Syst['F'])[:,0]
        """
    
    return u_mod_desacc, q_mod_desacc, u_mod_acc, q_mod_acc


def calcul_FRF(kb_desacc) :
    "lecture des paramètres d'entrée"
    kb_desacc = np.squeeze(kb_desacc) #si d==1
    N_a = len(kb_desacc)  #nombre d'aubes
    Modele = {
        'N_a' : N_a
    }
    
    [Modele, Sollic, Visu]=lecture(Modele)
    
    "Calcul direct de certaines quantités"
    Modele.update({
        'ω_b' : sqrt(Modele["k_b"]/Modele["m_b"]),  #fréquence nominal de l'aube :  ω_b**2 = k_b / m_b
        'k_c' : Modele["R"] * Modele["k_b"]         #raideur du couplage ω_c**2 = k_c / m_b et R = ω_c**2 / (k_b / m_b)
    })
    
    "-----------------------------------------------------------------------------------"
    "                        Construction du problème                                   "
    "-----------------------------------------------------------------------------------"
    "Système désaccordé"
    Syst = {
        'desacc_kb' : (kb_desacc - Modele["k_b"])/Modele["k_b"]   #désaccordage observé sur la roue aubagée δE
    }
    
    [M, K] = syst(N_a, Modele["m_b"], Modele["k_b"], Modele["k_c"], Syst["desacc_kb"] ) 
    
    Syst.update({
        'K' : K, 
        'M' : M
    })
    
    "Système accordé"    
    [M, K_acc] = syst(N_a, Modele["m_b"], Modele["k_b"], Modele["k_c"], np.zeros(N_a)) #matrice de masse inchangée
    Syst_acc = {
        'K' : K_acc, 
        'M' : M
    }

    "-----------------------------------------------------------------------------------"
    "                     Problèmes aux valeurs propres                                 "
    "-----------------------------------------------------------------------------------"    
    "Résolution du système aux valeurs propres (valp) : valp = ω**2 / équation (25) "
    [valp,vecp] = fct_vp(K,M)
    [valpa,vecpa] = fct_vp(K_acc,M)
    
    "Valeurs propres : ω_i"
    omega_i = (valp)**0.5
    omega_acc_i = (valpa)**0.5
    freq_valp = np.sort( omega_i /(2*pi) ) #fréquences : freq = sqrt(valp) /(2*pi)
    freq_valp_acc = np.sort( omega_acc_i /(2*pi) ) #fréquences : freq = sqrt(valp) /(2*pi)
    
    
    "Fréquences des valeurs propres : freq = sqrt(valp) /(2*pi)"
    Syst.update({ 
            'valp' : valp,
            'vecp' : vecp,
            'freq_valp' : freq_valp
    })
    Syst_acc.update({ 
            'valp' : valpa,
            'vecp' : vecpa,
            'freq_valp' : freq_valp_acc
    })


    "-----------------------------------------------------------------------------------"
    "                       Amortissement                                               "
    "-----------------------------------------------------------------------------------"
    choix = 3 #choix de la matrice d'amortissement
    
    "Matrice d'amortissement"
    if choix == 1:
        "1 : Amortissement de Rayleigh : viscoélastique"
        beta = 0.000001
        alpha = 0.00001
        Syst['C'] = alpha*Syst['M'] + beta * Syst['K']
        Syst['ksi'] = 0.5* (alpha / omega_i + beta * omega_i )
        
        Syst_acc['C'] = alpha*Syst_acc['M'] + beta * Syst_acc['K']
        Syst_acc['ksi'] = 0.5* (alpha / omega_acc_i + beta * omega_acc_i ) 
    elif choix == 2 :
        "2 : Amortissement structural : hystérétique"
        eta = 0.001*Sollic['ksi']
        Syst['C'] = eta * Syst['K']
        Syst['ksi'] = eta/2 * omega_i
        
        Syst_acc['C'] = eta * Syst_acc['K']
        Syst_acc['ksi'] = eta/2 * omega_acc_i
    else : 
        "3 : Amortissement modal fixé"
        ksi = Sollic['ksi'] * np.ones(N_a)
        C_mod = 2 * ksi * omega_i * np.eye(N_a)
        Phi = Syst['vecp'] / (np.diag(np.dot(np.dot(Syst['vecp'].T,Syst['M']),Syst['vecp']) ))**0.5
        invPhi = np.dot(Phi.T, Syst['M'])
        Syst['C'] = np.dot(np.dot(invPhi.T, C_mod), invPhi)
        Syst['ksi'] = ksi
        
        "système accordé"
        C_acc_mod = 2 * ksi * omega_acc_i * np.eye(N_a)
        Phi = Syst_acc['vecp'] / (np.diag(np.dot(np.dot(Syst_acc['vecp'].T,Syst_acc['M']),Syst_acc['vecp']) ))**0.5
        invPhi = np.dot(Phi.T, Syst_acc['M'])
        Syst_acc['C'] = np.dot(np.dot(invPhi.T, C_acc_mod), invPhi)
        Syst_acc['ksi'] = ksi
        
    "-----------------------------------------------------------------------------------"
    "                       Force appliquée aux aubes                                   "
    "-----------------------------------------------------------------------------------"
    "Force d'excitation : excitation tournante sur un régime moteur"
    r = Sollic['C'] #Sinha, 2005
    e = np.array([exp(1j * 2*pi*r*i/N_a) for i in range(0,N_a) ])
    F = Sollic['F_max'] * e
    Syst['F'] = F
    Syst_acc['F'] = F
    
    Entree = {
            'Modele':Modele,
            'Sollic':Sollic,
            'Syst_acc':{
                    'M':Syst_acc['M'],
                    'C':Syst_acc['C'],
                    'K':Syst_acc['K'],
                    'F':Syst_acc['F'],
                    #'ksi':Syst_acc['ksi']
                    },
            'Syst_desacc':{
                    'desacc_kb' : Syst['desacc_kb'],
                    'M':Syst['M'],
                    'C':Syst['C'],
                    'K':Syst['K'],
                    'F':Syst['F'],
                    #'ksi':Syst['ksi']
                    },
            }
    "-----------------------------------------------------------------------------------"
    "                       Décomposition dans la base modale                           "
    "-----------------------------------------------------------------------------------"
    
    "Système désaccordé : décomposition modale"
    [M_mod, K_mod, C_mod, F_mod, Phi] = fct_base_mod(Syst['vecp'], Syst['M'], Syst['K'], Syst['C'], Syst['F'])
    Syst.update({
        'M_mod' : M_mod,
        'K_mod' : K_mod,
        'C_mod' : C_mod,
        'F_mod' : F_mod,
        'Phi' : Phi
    })
        
    "Système accordé : décomposition modale"
    [M_modAc, K_modAc, C_modAc, F_modAc, PhiAc] = fct_base_mod(Syst_acc['vecp'], Syst_acc['M'], Syst_acc['K'], Syst_acc['C'], Syst_acc['F'])
    Syst_acc.update({
        'M_mod' : M_modAc,
        'K_mod' : K_modAc,
        'C_mod' : C_modAc,
        'F_mod' : F_modAc,
        'Phi' : PhiAc
    })
    
    "-----------------------------------------------------------------------------------"
    "                       Réponse forcée : par la base modale                         "
    "-----------------------------------------------------------------------------------"
    "Fréquences d'excitation : plusieurs options"
    FRF = 1 #choix du type de fréquences d'excitation
    """ 
        Remarque 1 : les fréquences d'excitation sont les mêmes pour le système accordé et désaccordé
        Remarque 2 : privilégier un balayage en fréquences car 
            1- les amplitudes maximales du système désaccordée ne sont pas aux fréquences propres du système accordé car on observe un décallage des fréquences propres du système désaccordé   (voir FA inférieur à 1 dans l'article de Sinha 2006)
            2- les amplitudes maximales ne sont pas forcement aux fréquences propres du système lorsque l'amortissement intervient 
        Recommandation : FRF = 1
    """
    if FRF == 1 :
        "1) Plage de fréquences "
        freq_plage_desacc = np.arange(0.90*freq_valp[0],1.05*freq_valp[-1],Visu['Ω_pas'])     #+/-5 % autour valp desacc
        freq_ajout_freqdesacc = np.append(freq_plage_desacc,freq_valp)         # plage freq desacc + valp desacc
        freq_ajout_freqacc = np.append(freq_ajout_freqdesacc,freq_valp_acc)    # plage freq desacc + valp desacc + valp acc
        freq_tir =  np.sort(freq_ajout_freqacc)
    elif FRF == 2 :
        "2) Fréquences propres du système accordée "
        freq_tir = freq_valp_acc
    elif FRF == 3 :
        "3) Fréquences propres du système désaccordée "
        freq_tir = freq_valp
    else : #Par défaut choix 4
        "4) Fréquences propres du système accordée et du système désaccordée"
        freq_tir = np.append(freq_valp_acc,freq_valp)
        
    Syst['freq_tir'] = freq_tir
    Syst_acc['freq_tir'] = freq_tir
    
    "Initialisation des matrices"
    nb_tir = len(freq_tir)
    u_mod_desacc = np.zeros((nb_tir, N_a), dtype = complex) #amplitude physique complexe
    u_mod_acc = np.zeros((nb_tir, N_a), dtype = complex)
    q_mod_desacc = np.zeros((nb_tir, N_a)) #amplitude physique réelle
    q_mod_acc = np.zeros((nb_tir, N_a))
    
    "Réponse forcée"
    [ u_mod_desacc, q_mod_desacc, u_mod_acc, q_mod_acc ] = fct_calcul_FRF(freq_tir, F_mod, F_modAc, Phi, PhiAc, Syst['ksi'], Syst_acc['ksi'], 
                omega_i, omega_acc_i, u_mod_desacc, q_mod_desacc, u_mod_acc, q_mod_acc)  
    
    Syst['q'] = q_mod_desacc
    Syst['u'] = u_mod_desacc
    Syst_acc['q'] = q_mod_acc
    Syst_acc['u'] = u_mod_acc
    
    "Facteur d'amplification"
    q_mod_desacc_max_aube = np.max(q_mod_desacc, axis=0) 
    envelop_q_mod_desacc =  np.max(q_mod_desacc, axis=1)
    q_mod_acc_max_aube = np.max(q_mod_acc, axis=0) 
    q_mod_acc_max = np.max(q_mod_acc_max_aube)
    factAmpAubes = q_mod_desacc_max_aube / q_mod_acc_max
    factAmp = np.sort( factAmpAubes )
    
    Syst['q_max_aube'] = q_mod_desacc_max_aube
    Syst['envelop_q_max'] = envelop_q_mod_desacc
    Syst['freq_max_aube'] = np.where(q_mod_desacc_max_aube == q_mod_desacc) 
    Syst_acc['q_max_aube'] = q_mod_acc_max
    Syst_acc['freq_max_aube'] =  np.where(q_mod_acc_max == q_mod_acc) 
    Syst['fact_ampli_aubes'] = factAmpAubes
    Syst['fact_ampli_roue'] = factAmp[-1]
    
    Sortie = {'Syst_acc' : Syst_acc, 'Syst_desacc' : Syst } 
    """
        Certains résultats théoriques  
        
        ksi_acc = Syst_acc['ksi']
        ksi_desacc = Syst['ksi']
        Modele['C'] = 1
        q_theo_acc_max = Sollic["F_max"] /  Modele['k_b'] * 1 / ( 2 * ksi_acc *( 1 + 2*Modele['R']*(1-np.cos(2*pi*Modele['C']/N_a)) - ksi_acc**2)**0.5)
        print( abs(q_theo_acc_max - q_mod_acc_max))
        if Modele['R'] == 0 : #desaccordé + sans couplage
            q_theo_desacc_max = Sollic["F_max"] /  Modele['k_b'] * 1 / ( 2 * ksi_desacc *( 1 + Syst['desacc_kb'] - ksi**2)**0.5)
    
    """
    return [Entree,Sortie]
