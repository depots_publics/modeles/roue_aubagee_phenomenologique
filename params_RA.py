# -*- coding: utf-8 -*-
"""
Paramètres d'entrée du modèle de la roue aubagee phénoménologique

@author: jdreau
"""

"-----------------------------------------------------------------------------------"
"                    Initialisation des paramètres                                  "
"-----------------------------------------------------------------------------------"
"                    Paramètres Sinha, 2006                                         "
"   10 aubes                                                                        "
"   Désaccordage appliqué sur la raideur de l'aube : k_b                            "

kc =  45430     #raideur du ressort de couplage [N/m] 
c = 1.43        #coefficient d'amortissement [N-sec/m] 
Modele.update({
     'k_b' :4.3e5,            # raideur du ressort de torsion de l'aube [N/m] 
     'm_b' : 0.0114,          # masse de l'aube [kg] 
     'R' : kc/4.3e5,          # couplage : k_c / k_b = ω_c**2 / ω_b**2
})

Sollic = {
	'F_max' : 1,			# maximum de la norme de la force appliquée [N]	
	'C' : 3,				# ordre moteur de la force appliquée aux aubes
     'ksi' : c/(2*Modele['m_b']*sqrt(Modele['k_b']/Modele['m_b'])) # taux d'amortissement  ζ
}

Visu = {
	'Ω_pas' : 0.2,		# pas pour la FRF (fin : 0.01) 
}
