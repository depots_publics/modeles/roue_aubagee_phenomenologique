# -*- coding: utf-8 -*-
"""

Programme de lancement du calcul de la réponse forcée d'une roue aubagée phénoménologique avec prise en compte du désaccordage

@author: jdreau
"""

"Modules"
import numpy as np 
import matplotlib.pyplot as plt
import pandas as pd
import scipy.io as sio

from calculs_modele_RA import calcul_FRF


"-----------------------------------------------------------------------------------"
"                            Lancement du programme                                 "
"-----------------------------------------------------------------------------------"
"1) Paramètres de la roue : voir params_RA"

"2) Désaccordage de la raideur des aubes : valeur nominale 4.3e5"
kb_desacc = np.array([407964.23120371, 451137.07377074, 402954.32724671, 429656.35292189,
       441846.20452404, 424310.90801479, 421580.66929849, 441029.21631729,
       417572.58544433, 449584.61982215])

"3) Calcul de la réponse forcée"
[Entree, Sortie] = calcul_FRF(kb_desacc)
Syst_acc = Sortie['Syst_acc']
Syst_desacc = Sortie['Syst_desacc']

"-----------------------------------------------------------------------------------"
"                               Gestion des sorties                                 "
"-----------------------------------------------------------------------------------"
"1) Sauvegarde des dictionnaires"
nom_sortie = 'res_modele_RApheno.mat'
sio.savemat(nom_sortie,Sortie,long_field_names=True) #long_field_names =True : augmentation de la limite de nom d'un champ à 63 caractères

    
"2) Figure Matplotlib: fonction de réponse en fréquences"
plt.figure()
plt.plot(Syst_desacc['freq_tir'],Syst_desacc['q'],'bisque')
plt.plot(Syst_desacc['freq_tir'],Syst_desacc['envelop_q_max'],'orange')
plt.plot(Syst_acc['freq_tir'], Syst_acc['q'],'k')
plt.xlabel(r"Frequence d'excitation (Hz)")
plt.ylabel(r"Amplitude de vibration (m)")
plt.ylim(0,)
plt.show()
"sauvegarde figure : plt.savefig(nom_fig)"


"3) Figure Tikz : fonction de réponse en fréquences"
" Hypothèse pour avoir 1 seul fichier résultats : fréquences d'excitation identiques pour les deux systèmes"

"a) amplitudes système accordé"
res_acc = np.hstack((Syst_acc['freq_tir'][:,None],Syst_acc['q']))

"b) amplitudes système désaccordé"
res_desacc = np.hstack((Syst_desacc['freq_tir'][:,None],Syst_desacc['q'],Syst_desacc['envelop_q_max'][:,None]))

"c) toutes les amplitudes "
res = np.hstack((res_acc,res_desacc[:,1:]))

"c) enregistrement "
nom_txt = 'modele_RApheno_FRF.txt'
columns = np.array(['FreqsTir'] + ['Acc_Aube'+str(i) for i in range(1,11)] + ['Desacc_Aube'+str(i) for i in range(1,11)] + ['Desacc_envelop'])
tab = pd.DataFrame(data=res,columns=columns)
pd.set_option('precision', 13)
fichier = open(nom_txt,'w')
fichier.write('index '+tab.to_string())
fichier.close()

